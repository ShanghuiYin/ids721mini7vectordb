
# IDS721 Spring 2024 Weekly Mini Project 7 Data Processing with Vector Database

## Requirements
1. Ingest data into Vector database 
2. Perform queries and aggregations 
3. Visualize output


## Setup
1. Install Rust
2. Start by creating a new binary-based Cargo project and changing into the new directory:
   `cargo new project_name && cd project_name`
3. download the latest Qdrant image from Dockerhub `docker pull qdrant/qdrant`

4. Run Qdrant with enabled gRPC interface
```dockerfile
# With env variable
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```

5. Implement the logic in `main.rs` file
6. Add the dependencies in `Cargo.toml` file by
   `cargo add qdrant-client anyhow tonic tokio serde-json --features tokio/rt-multi-thread`
7. Run the project `cargo run`

## Deliverables

### Run Qdrant Vector Database
![img.png](img.png)

### Data ingestion:
Generates and ingests five points into the "test" collection. 
Each point consists of a randomly generated 10-dimensional vector (of f32 values) 
and a structured payload containing a name, value, and group. 
This step demonstrates how to insert complex data into a vector database, 
allowing for later retrieval based on vector similarity or payload content.


### Query functionality:
After ingesting data, the code performs a search query within the "test" collection. 
The query doesn't use any specific filters, 
allowing it to retrieve a broad set of points based on vector similarity 
to the provided query vector. The query vector in this example is a 10-dimensional vector 
filled with the value 50.0 (of type f32), 
and the search is limited to the top 10 most similar points.

### Visualizations:

> format the output
>
![img_1.png](img_1.png)

> visualize the output in the qdrant web interface

![img_2.png](img_2.png)


## Reference
1. https://www.cargo-lambda.info/guide/getting-started.html
2. https://github.com/qdrant/rust-client
3. https://qdrant.tech/documentation/quick-start/

